# Soprano Aldente

> musical harmony automatization

A project for [Social Startup Hackathon](https://vdk.te-st.ru).

Presentation [on SlideShare](https://www.slideshare.net/grawlcore/soprano-aldente-in-russian) (in russian)

## Requirements

- Ruby on Rails to run backend
- Node.js & Yarn to build frontend

### Start Rails server

```bash
rails s -p 7896
```

### Start Webpack Dev Server for Vue development

```bash
yarn hot
```

### If something was changed on backend and not working properly, try following

```bash
bundle install
rake db:migrate
```