const webpack = require('webpack')
const mix = require('laravel-mix')
const autoprefixer = require('autoprefixer')
const postcssAxis = require('postcss-axis')
const postcssShort = require('postcss-short')
const postcssPositionAlt = require('postcss-position-alt')
const postcssWillChange = require('postcss-will-change')
mix.setPublicPath('public')
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for your application, as well as bundling up your JS files.
 |
 */
const prod = process.env.NODE_ENV === 'production'
mix.js('app/vue/app.js', prod ? 'public/production' : 'public/client')
if (prod) mix.babel('public/production/app.js', 'public/production/app.js')
mix.options({
    postCss: [
        autoprefixer,
        postcssAxis,
        postcssShort,
        postcssPositionAlt,
		postcssWillChange,
    ]
})
mix.webpackConfig({
	devServer: {
		proxy: {
			"**": 'http://localhost:7896'
		},
	},
})
mix.browserSync({
    port: 4634,
    proxy: 'http://localhost:7896',
    open: false,
    files: [
        'app/**/*',
    ]
})


// Full API
// mix.js(src, output);
// mix.react(src, output); <-- Identical to mix.js(), but registers React Babel compilation.
// mix.extract(vendorLibs);
// mix.sass(src, output);
// mix.standaloneSass('src', output); <-- Faster, but isolated from Webpack.
// mix.less(src, output);
// mix.stylus(src, output);
// mix.browserSync('my-site.dev');
// mix.combine(files, destination);
// mix.babel(files, destination); <-- Identical to mix.combine(), but also includes Babel compilation.
// mix.copy(from, to);
// mix.copyDirectory(fromDir, toDir);
// mix.minify(file);
// mix.sourceMaps(); // Enable sourcemaps
// mix.version(); // Enable versioning.
// mix.disableNotifications();
// mix.setPublicPath('path/to/public');
// mix.setResourceRoot('prefix/for/resource/locators');
// mix.autoload({}); <-- Will be passed to Webpack's ProvidePlugin.
// mix.webpackConfig({}); <-- Override webpack.config.js, without editing the file directly.
// mix.then(function () {}) <-- Will be triggered each time Webpack finishes building.
// mix.options({
//   extractVueStyles: false, // Extract .vue component styling to file, rather than inline.
//   processCssUrls: true, // Process/optimize relative stylesheet url()'s. Set to false, if you don't want them touched.
//   purifyCss: false, // Remove unused CSS selectors.
//   uglify: {}, // Uglify-specific options. https://webpack.github.io/docs/list-of-plugins.html#uglifyjsplugin
//   postCss: [] // Post-CSS options: https://github.com/postcss/postcss/blob/master/docs/plugins.md
// });
