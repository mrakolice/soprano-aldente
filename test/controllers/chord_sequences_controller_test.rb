require 'test_helper'

class ChordSequencesControllerTest < ActionController::TestCase
  setup do
    @chord_sequence = chord_sequences(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:chord_sequences)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create chord_sequence" do
    assert_difference('ChordSequence.count') do
      post :create, chord_sequence: { first_chord_id: @chord_sequence.first_chord_id, second_chord_id: @chord_sequence.second_chord_id, third_chord_id: @chord_sequence.third_chord_id }
    end

    assert_redirected_to chord_sequence_path(assigns(:chord_sequence))
  end

  test "should show chord_sequence" do
    get :show, id: @chord_sequence
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @chord_sequence
    assert_response :success
  end

  test "should update chord_sequence" do
    patch :update, id: @chord_sequence, chord_sequence: { first_chord_id: @chord_sequence.first_chord_id, second_chord_id: @chord_sequence.second_chord_id, third_chord_id: @chord_sequence.third_chord_id }
    assert_redirected_to chord_sequence_path(assigns(:chord_sequence))
  end

  test "should destroy chord_sequence" do
    assert_difference('ChordSequence.count', -1) do
      delete :destroy, id: @chord_sequence
    end

    assert_redirected_to chord_sequences_path
  end
end
