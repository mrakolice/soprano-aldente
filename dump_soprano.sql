-- MySQL dump 10.13  Distrib 5.7.18, for Linux (x86_64)
--
-- Host: zwgaqwfn759tj79r.chr7pe7iynqr.eu-west-1.rds.amazonaws.com    Database: u897v8wy2p6ybkar
-- ------------------------------------------------------
-- Server version	5.7.11-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `chord_sequences`
--

DROP TABLE IF EXISTS `chord_sequences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chord_sequences` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_chord_id` int(11) DEFAULT NULL,
  `second_chord_id` int(11) DEFAULT NULL,
  `third_chord_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `fourth_chord_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_rails_5e150d5a62` (`first_chord_id`),
  KEY `fk_rails_43aa878fe1` (`second_chord_id`),
  KEY `fk_rails_22528f0986` (`third_chord_id`),
  KEY `fk_rails_3fcb4aa4fb` (`fourth_chord_id`),
  CONSTRAINT `fk_rails_22528f0986` FOREIGN KEY (`third_chord_id`) REFERENCES `chords` (`id`),
  CONSTRAINT `fk_rails_3fcb4aa4fb` FOREIGN KEY (`fourth_chord_id`) REFERENCES `chords` (`id`),
  CONSTRAINT `fk_rails_43aa878fe1` FOREIGN KEY (`second_chord_id`) REFERENCES `chords` (`id`),
  CONSTRAINT `fk_rails_5e150d5a62` FOREIGN KEY (`first_chord_id`) REFERENCES `chords` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chord_sequences`
--

LOCK TABLES `chord_sequences` WRITE;
/*!40000 ALTER TABLE `chord_sequences` DISABLE KEYS */;
INSERT INTO `chord_sequences` VALUES (2,1,4,7,'2017-04-28 16:40:44','2017-04-28 16:40:44',NULL),(4,1,4,NULL,'2017-04-29 08:23:04','2017-04-29 08:23:04',NULL),(5,7,1,NULL,'2017-04-29 08:23:23','2017-04-29 08:23:23',NULL),(6,1,9,2,'2017-04-29 08:30:26','2017-04-29 08:30:26',NULL),(7,1,14,2,'2017-04-29 08:32:24','2017-04-29 08:32:24',NULL),(8,4,3,5,'2017-04-29 08:32:42','2017-04-29 08:32:42',NULL),(9,4,15,2,'2017-04-29 08:33:22','2017-04-29 08:33:22',NULL),(10,5,13,1,'2017-04-29 08:34:07','2017-04-29 08:34:07',NULL),(11,7,15,2,'2017-04-29 08:34:24','2017-04-29 08:34:24',NULL),(12,8,13,1,'2017-04-29 08:34:37','2017-04-29 08:34:37',NULL),(13,2,9,1,'2017-04-29 08:35:14','2017-04-29 08:35:14',NULL),(14,2,14,1,'2017-04-29 08:35:30','2017-04-29 08:35:30',NULL),(15,5,3,4,'2017-04-29 08:35:49','2017-04-29 08:36:18',NULL),(16,4,59,10,'2017-04-29 14:14:29','2017-04-29 14:14:29',12),(17,5,59,10,'2017-04-29 14:14:50','2017-04-29 14:14:50',12),(18,1,7,NULL,'2017-04-29 15:13:01','2017-04-29 15:13:01',NULL),(19,4,1,NULL,'2017-04-29 17:57:03','2017-04-29 17:57:03',NULL),(20,1,5,NULL,'2017-04-29 18:12:44','2017-04-29 18:12:44',NULL),(22,2,4,NULL,'2017-04-29 18:14:14','2017-04-29 18:14:14',NULL),(24,4,2,NULL,'2017-04-29 18:15:31','2017-04-29 18:15:31',NULL),(26,5,1,NULL,'2017-04-29 18:15:50','2017-04-29 18:15:50',NULL),(28,4,8,NULL,'2017-04-29 18:17:07','2017-04-29 18:17:07',NULL),(30,5,7,NULL,'2017-04-29 18:17:27','2017-04-29 18:17:27',NULL),(32,7,2,NULL,'2017-04-29 18:18:34','2017-04-29 18:18:34',NULL),(34,8,1,NULL,'2017-04-29 18:18:53','2017-04-29 18:18:53',NULL),(36,1,8,NULL,'2017-04-29 18:20:08','2017-04-29 18:20:08',NULL),(38,2,7,NULL,'2017-04-29 18:20:54','2017-04-29 18:20:54',NULL),(40,1,2,10,'2017-04-30 00:44:36','2017-04-30 00:44:36',12),(41,1,2,NULL,'2017-04-30 03:28:06','2017-04-30 03:28:06',NULL),(43,1,1,NULL,'2017-04-30 03:29:39','2017-04-30 03:29:39',NULL),(44,2,2,NULL,'2017-04-30 03:30:19','2017-04-30 03:30:19',NULL),(45,4,4,NULL,'2017-04-30 03:30:30','2017-04-30 03:30:30',NULL),(46,5,5,NULL,'2017-04-30 03:30:38','2017-04-30 03:30:38',NULL),(47,7,7,NULL,'2017-04-30 03:30:47','2017-04-30 03:30:47',NULL),(48,4,5,NULL,'2017-04-30 03:31:05','2017-04-30 03:31:05',NULL),(49,7,8,NULL,'2017-04-30 03:31:45','2017-04-30 03:31:45',NULL),(50,8,8,NULL,'2017-04-30 03:31:53','2017-04-30 03:31:53',NULL),(51,2,7,NULL,'2017-04-30 04:04:08','2017-04-30 04:04:08',NULL);
/*!40000 ALTER TABLE `chord_sequences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `chords`
--

DROP TABLE IF EXISTS `chords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `chords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `parent_chord_id` int(11) DEFAULT NULL,
  `soprano` int(11) DEFAULT '1',
  `alt` int(11) DEFAULT '1',
  `tenor` int(11) DEFAULT '1',
  `bass` int(11) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `index_chords_on_parent_chord_id` (`parent_chord_id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `chords`
--

LOCK TABLES `chords` WRITE;
/*!40000 ALTER TABLE `chords` DISABLE KEYS */;
INSERT INTO `chords` VALUES (1,'T',0,'2017-04-28 16:00:06','2017-04-29 14:42:00',NULL,5,3,1,1),(2,'T6',0,'2017-04-28 16:00:25','2017-04-29 14:42:00',NULL,1,5,1,3),(3,'T64',0,'2017-04-28 16:00:43','2017-04-29 14:42:00',NULL,3,1,1,5),(4,'S',0,'2017-04-28 16:01:11','2017-04-29 14:42:00',NULL,1,6,4,4),(5,'S6',0,'2017-04-28 16:02:53','2017-04-29 14:42:00',NULL,4,4,1,6),(6,'S64',0,'2017-04-28 16:03:06','2017-04-29 14:42:00',NULL,6,4,4,1),(7,'D',0,'2017-04-28 16:03:20','2017-04-29 14:42:00',NULL,2,7,5,5),(8,'D6',0,'2017-04-28 16:03:36','2017-04-29 14:42:00',NULL,5,2,5,7),(9,'D64',0,'2017-04-28 16:03:55','2017-04-29 14:42:00',NULL,7,5,5,2),(10,'D7',0,'2017-04-29 08:17:39','2017-04-29 14:42:00',NULL,4,2,7,5),(12,'T3',0,'2017-04-29 08:24:16','2017-04-29 14:42:00',NULL,3,1,1,1),(13,'D65',0,'2017-04-29 08:31:15','2017-04-29 14:42:00',NULL,5,4,2,7),(14,'D43',0,'2017-04-29 08:31:36','2017-04-29 14:42:00',NULL,7,5,4,2),(15,'D2',0,'2017-04-29 08:31:53','2017-04-29 14:42:00',NULL,2,7,5,4),(16,'T (т1)',0,'2017-04-29 09:01:28','2017-04-29 14:42:00',1,1,5,3,1),(17,'T (т3)',0,'2017-04-29 09:02:17','2017-04-29 14:42:00',1,3,1,5,1),(18,'T (ш1)',1,'2017-04-29 09:03:37','2017-04-29 14:42:00',1,1,3,5,1),(19,'T (ш3)',1,'2017-04-29 09:05:40','2017-04-29 14:42:00',1,3,5,1,1),(20,'T (ш5)',1,'2017-04-29 09:06:12','2017-04-29 14:42:00',1,5,1,3,1),(21,'T6 (ш5)',1,'2017-04-29 09:13:02','2017-04-29 14:42:00',2,5,1,1,3),(22,'T64 (ш1)',1,'2017-04-29 09:22:24','2017-04-29 14:42:00',3,1,3,1,5),(23,'S (т1)',0,'2017-04-29 09:27:22','2017-04-29 14:42:00',4,4,1,6,4),(24,'S (т3)',0,'2017-04-29 09:28:04','2017-04-29 14:42:00',4,6,4,1,4),(25,'S (ш1)',1,'2017-04-29 09:28:47','2017-04-29 14:42:00',4,4,6,1,4),(26,'S (ш3)',1,'2017-04-29 09:29:30','2017-04-29 14:42:00',4,6,1,4,4),(27,'S (ш5)',1,'2017-04-29 09:30:05','2017-04-29 14:42:00',4,1,4,6,4),(28,'T6 (т1)',0,'2017-04-29 09:33:44','2017-04-29 14:42:00',2,1,1,5,3),(30,'S6 (ш5)',1,'2017-04-29 09:42:04','2017-04-29 14:42:00',5,1,4,4,6),(31,'S64 (ш1)',1,'2017-04-29 10:01:30','2017-04-29 14:42:00',6,4,6,4,1),(32,'D (т1)',NULL,'2017-04-29 12:33:18','2017-04-29 14:42:00',7,5,2,7,5),(33,'D (т3)',NULL,'2017-04-29 12:33:59','2017-04-29 14:42:00',7,7,5,2,5),(34,'D (ш1)',1,'2017-04-29 12:39:56','2017-04-29 14:42:00',7,5,7,2,5),(35,'D (ш3)',1,'2017-04-29 12:46:32','2017-04-29 14:42:00',7,7,2,5,5),(36,'D (ш5)',1,'2017-04-29 12:55:50','2017-04-29 14:42:00',7,2,5,7,5),(37,'D6 (ш5)',1,'2017-04-29 12:57:51','2017-04-29 14:42:00',8,2,5,5,7),(38,'D64 (ш1)',1,'2017-04-29 12:59:12','2017-04-29 14:42:00',9,5,7,5,2),(40,'D7 (т5)',NULL,'2017-04-29 13:09:48','2017-04-29 14:42:00',10,2,7,4,5),(42,'D7 (ш5)',1,'2017-04-29 13:11:05','2017-04-29 14:42:00',10,2,4,7,5),(43,'D7 (ш7)',1,'2017-04-29 13:11:38','2017-04-29 14:42:00',10,4,7,2,5),(44,'D65 (т7)',0,'2017-04-29 13:29:20','2017-04-29 14:42:00',13,4,2,5,7),(45,'D65 (ш1)',1,'2017-04-29 13:30:32','2017-04-29 14:42:00',13,5,2,4,7),(46,'D65 (ш5)',1,'2017-04-29 13:31:13','2017-04-29 14:42:00',13,2,5,4,7),(48,'D43 (т1)',0,'2017-04-29 13:44:25','2017-04-29 14:42:00',14,5,4,7,2),(49,'D43 (ш1)',1,'2017-04-29 13:52:38','2017-04-29 14:42:00',14,5,7,4,2),(50,'D43 (ш3)',1,'2017-04-29 13:54:47','2017-04-29 14:42:00',14,7,4,5,2),(51,'D43 (ш7)',1,'2017-04-29 13:55:28','2017-04-29 14:42:00',14,4,7,5,2),(52,'D2 (т1)',0,'2017-04-29 13:58:36','2017-04-29 14:42:00',15,5,2,7,4),(53,'D2 (т3)',0,'2017-04-29 13:59:10','2017-04-29 14:42:00',15,7,5,2,4),(54,'D2 (ш1)',1,'2017-04-29 14:01:03','2017-04-29 14:42:00',15,5,7,2,4),(55,'D2 (ш3)',1,'2017-04-29 14:02:16','2017-04-29 14:42:00',15,7,2,5,4),(56,'D2 (ш5)',1,'2017-04-29 14:03:09','2017-04-29 14:42:00',15,2,5,7,4),(57,'T3 (т1)',0,'2017-04-29 14:06:48','2017-04-29 14:42:01',12,1,1,3,1),(58,'T3 (ш1)',1,'2017-04-29 14:07:20','2017-04-29 14:42:01',12,NULL,NULL,NULL,1),(59,'K',0,'2017-04-29 14:10:10','2017-04-29 14:42:01',NULL,1,5,3,5),(60,'K (т3)',0,'2017-04-29 14:11:10','2017-04-29 14:42:01',59,3,1,5,5),(61,'K (т5)',0,'2017-04-29 14:11:51','2017-04-29 14:42:01',59,5,3,1,5),(62,'K (ш1)',1,'2017-04-29 14:12:28','2017-04-29 14:42:01',59,1,3,5,5),(63,'K (ш3)',1,'2017-04-29 14:13:29','2017-04-29 14:42:01',59,3,5,1,5),(64,'K (ш5)',1,'2017-04-29 14:14:00','2017-04-29 14:42:01',59,5,1,3,5),(65,'D64 (ш5)',1,'2017-04-29 17:59:37','2017-04-29 17:59:37',9,2,5,7,2),(66,'T64 (ш5)',1,'2017-04-29 18:01:51','2017-04-29 18:01:51',NULL,5,1,3,5);
/*!40000 ALTER TABLE `chords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_migrations`
--

DROP TABLE IF EXISTS `schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schema_migrations` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  UNIQUE KEY `unique_schema_migrations` (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_migrations`
--

LOCK TABLES `schema_migrations` WRITE;
/*!40000 ALTER TABLE `schema_migrations` DISABLE KEYS */;
INSERT INTO `schema_migrations` VALUES ('20170428025644'),('20170428054436'),('20170428154949'),('20170428155018'),('20170428161639'),('20170429131028'),('20170429142029'),('20170429142256'),('20170429142839'),('20170501021800');
/*!40000 ALTER TABLE `schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tonalities`
--

DROP TABLE IF EXISTS `tonalities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tonalities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_unicode_ci,
  `sequence` text COLLATE utf8_unicode_ci,
  `is_major` tinyint(1) DEFAULT '1',
  `offset` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tonalities`
--

LOCK TABLES `tonalities` WRITE;
/*!40000 ALTER TABLE `tonalities` DISABLE KEYS */;
INSERT INTO `tonalities` VALUES (1,'Ля минор','A,B,C,D,E,F,G#',0,6),(2,'До мажор','C,D,E,F,G,A,B',1,0);
/*!40000 ALTER TABLE `tonalities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `encrypted_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `reset_password_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `sign_in_count` int(11) NOT NULL DEFAULT '0',
  `current_sign_in_at` datetime DEFAULT NULL,
  `last_sign_in_at` datetime DEFAULT NULL,
  `current_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_sign_in_ip` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_users_on_email` (`email`),
  UNIQUE KEY `index_users_on_reset_password_token` (`reset_password_token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-05-09 23:34:34
