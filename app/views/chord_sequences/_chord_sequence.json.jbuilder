json.extract! chord_sequence, :id, :first_chord_id, :second_chord_id, :third_chord_id, :created_at, :updated_at
json.url chord_sequence_url(chord_sequence, format: :json)