class ChordsController < ApplicationController
  load_and_authorize_resource
  before_action :set_parent_chords

  def index
    @chords = Chord.all.order(:name)
    render :index
  end

  def new
    render :new
  end

  def create
    if @chord.save
      redirect_to chords_path
    else
      render :new
    end
  end

  def update
    if @chord.update_attributes(chord_params)
      redirect_to chords_path
    else
      render :edit
    end
  end

  def destroy
    @chord.destroy
    redirect_to chords_path
  end

  private

  def set_parent_chords
    @parent_chords = Chord.where(:parent_chord_id => nil)
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def chord_params
    params.require(:chord).permit(:name, :soprano, :alt, :tenor, :bass, :position, :parent_chord_id)
  end
end
