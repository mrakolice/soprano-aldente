class ChordSequencesController < ApplicationController
  load_and_authorize_resource
  before_action :set_chords

  def index
    @chord_sequences = ChordSequence.order(:first_chord_id, :second_chord_id, :third_chord_id)
    render :index
  end

  def new
    render :new
  end

  def create
    if @chord_sequence.save
      redirect_to chord_sequences_path
    else
      render :new
    end
  end

  def update
    if @chord_sequence.update_attributes(chord_sequence_params)
      redirect_to chord_sequences_path
    else
      render :edit
    end
  end

  def destroy
    @chord_sequence.destroy
    redirect_to chord_sequences_path
  end

  private
    def set_chords
      @chords = Chord.where(:parent_chord_id => nil)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def chord_sequence_params
      params.require(:chord_sequence).permit(:first_chord_id, :second_chord_id, :third_chord_id, :fourth_chord_id, :position)
    end
end
