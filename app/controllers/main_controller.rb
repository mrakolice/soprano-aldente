class MainController < ApplicationController
  layout 'application'

  def index
    render :index
  end
end
