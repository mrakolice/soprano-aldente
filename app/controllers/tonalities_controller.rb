class TonalitiesController < ApplicationController
  load_and_authorize_resource
  def index
    render :index
  end

  def new
    render :new
  end

  def create
    if @tonality.save
      redirect_to tonalities_path
    else
      render :new
    end
  end

  def update
    if @tonality.update_attributes(tonality_params)
      redirect_to tonalities_path
    else
      render :edit
    end
  end

  def destroy
    @tonality.destroy
    redirect_to tonalities_path
  end

  private

  def tonality_params
    params.require(:tonality).permit(:name, :sequence, :is_major, :offset)
  end
end
