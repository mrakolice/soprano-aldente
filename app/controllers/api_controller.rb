class ApiController < ApplicationController
  skip_before_action :verify_authenticity_token
  respond_to :json

  def get_chords
    puts params
    notes = params[:notes]
    tonality_id = params[:tonality_id]

    result = ClassicHarmonyAnalizer.new(tonality_id).analyze(notes)

    render json: {
        variants: [
            result
        ]
    }
  end

  def get_chords_new
    notes = params[:notes]
    tonality_id = params[:tonality_id]
    all_variants = params[:all_variants]

    result = GraphHarmonyAnalizer.new(tonality_id).analyze(notes, all_variants)

    render json: {
        variants: result
    }
  end

  def get_tonalities
    render json: Tonality.all.each
  end

  def get_test_chords
       render json: {
            variants: get_variants
        }
  end

  def get_variants
    [
        {
            :soprano => ['c4', 'd4', 'e4', 'b4'],
            :alt => [ 'g3', 'b3', 'c4', 'e4'],
            :tenor => ['e3', 'g3', 'g3', 'b3'],
            :bass => ['c3', 'g2', 'c3', 'g3'],
            :chords => ["T (т3)","S (т1)","T","S (т3)"]
        },
        {
            :soprano => ["c5","d5","e5","f5","c5","d5","e5","f5"],
            :alt =>     ["g4","g4","c5","b4","c4","g4","c5","f4"],
            :tenor =>   ["c4","b3","g4","g4","g3","b3","g4","c4"],
            :bass =>    ["e3","d3","c4","d4","e3","d3","c4","a3"],
            :chords =>  ["T6","D64 (ш5)","T (т3)","D43 (ш7)","T6 (т1)","D64 (ш5)","T (т3)","S6"]
        },
        {
            :soprano => ["c5","d5","e5","f5","c5","d5","e5","f5","c5","d5","e5","f5","c5","d5","e5","f5"],
            :alt =>     ["g4","g4","c5","b4","c4","g4","c5","f4","g4","g4","c5","b4","c4","g4","c5","f4"],
            :tenor =>   ["c4","b3","g4","g4","g3","b3","g4","c4","c4","b3","g4","g4","g3","b3","g4","c4"],
            :bass =>    ["e3","d3","c4","d4","e3","d3","c4","a3","e3","d3","c4","d4","e3","d3","c4","a3"],
            :chords =>  ["T6","D64 (ш5)","T (т3)","D43 (ш7)","T6 (т1)","D64 (ш5)","T (т3)","S6","T6","D64 (ш5)","T (т3)","D43 (ш7)","T6 (т1)","D64 (ш5)","T (т3)","S6"]
        }
    ]
  end
end
