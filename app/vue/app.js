import Vue from 'vue'
import mainComponent from './components/main'
new Vue({
	name: 'root',
	el: '#app',
	data: {
	},
	components: {
		mainComponent,
	},
	mounted() {
		console.log('Vue application is mounted')
	},
})
