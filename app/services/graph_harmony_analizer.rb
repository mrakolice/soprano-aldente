#encoding:utf-8
require 'benchmark'

class GraphHarmonyAnalizer < HarmonyAnalizer
  def initialize(tonality_id)
    @tonality = Tonality.find(tonality_id)
    @is_minor = !@tonality.is_major
    @available_first_chords = [
        Chord.find_by_name('T'),
    # Chord.find_by_name('T6'),
    ]
    @sequences = ChordSequence.get_all_sequences
  end

  def analyze(notes, all_variants=false)
    result = []
    Benchmark.bm do |bm|
      prepared_chords = []
      bm.report('notes map') do
        prepared_chords = notes.map {|note|
          chords = Chord.where(:soprano => @tonality.get_stage(note)).to_a
          chords.map {|c| c.for_tonality(@tonality).with_soprano_octave(note[-1]).as_json}
        }
      end
      chords = []
      bm.report('find sequences all') do
        prepared_chords[0].each {|chord|
          result = []
          bm.report('find_sequences_recursive') do
            result = find_sequences_recursive(chord, prepared_chords.drop(1))
          end
          bm.report('sequences delete if') do
            result.delete_if {|c| c.nil? or c.is_a?(Array) and c.empty?}
          end
          chords << result
        }
      end
      bm.report('chords delete if') do
        chords.delete_if {|c| c.empty?}
      end

      bm.report('calculate paths') do
        chords.each {|cc|
          cc.map {|c|
            paths(c) {|p|
              result << p
            }
          }
        }
      end

      bm.report('result delete if') do
        result.delete_if {|v| v.length != notes.length} unless all_variants
      end
      bm.report('result map to gui chords') do
        result = result.map {|result_chords|
          {
              :soprano => result_chords.map {|c| c[:soprano_note]},
              :alt => result_chords.map {|c| c[:alt_note]},
              :tenor => result_chords.map {|c| c[:tenor_note]},
              :bass => result_chords.map {|c| c[:bass_note]},
              :chords => result_chords.map {|c| c[:name]}
          }
        }
      end
    end
    result
  end

  def paths(node, path=[], &proc)
    # puts "#{node.as_json({})}"
    # Давай напишем алгоритм обхода, чтобы было понятно, что надо сделать
    # Сначала мы добавляем последний аккорд, если его не было в пути до этого
    # Потом мы добавляем ВСЕ ВОЗМОЖНЫЕ ВАРИАНТЫ ПОСЛЕДОВАТЕЛЬНОСТЕЙ
    # И ДЛЯ КАЖДОГО ВАРИАНТА последнего аккорда мы добавляем возможные варианты последовательностей
    # Причем варианты последовательностей соотносятся с номером последовательности
    # Иначе говоря, для вариантов sequence: [ [A, B], [C, D] ], children: [ C, D ]
    # Сгенерироваться должно A -> C - заход в C, B -> C - заход в C, A -> D - заход в D, B -> D - заход в D
    # Или просто декартово умножение множеств для sequence, а потом бежим по Children с соответствием последнему аккорду

    path << node[:chord] unless path[-1] == node[:chord]
    if node[:sequence].empty? and node[:children].empty?
      proc.call(path)
    else
      variants = node[:sequence].map {|s| s.is_a?(Array) ? s.flatten : s}
      # puts "variants before product, #{variants}"
      if variants[0].is_a? Array
        variants = variants[0].product(*variants[1..-1])
      else
        variants = [variants]
      end
      variants.each do |variant|
        path_new = path.map {|a| a}
        # variant = variant.flatten if variant.is_a? Array
        # puts "#{variant}"
        path_new.concat(variant.map {|v| v[:chord]})
        child = node[:children].flatten
        if child.empty?
          proc.call(path_new)
        else
          child = child.find {|c| c[:chord] == variant[-1][:chord]}
          paths(child, path_new.map {|a| a}, &proc)
        end
      end
    end
  end

  def find_sequences_recursive(chord, chords)
    sequences = get_sequences_for_chord(chord)
    result = []

    # puts "chord #{chord}"

    sequences.each do |sequence|
      actual_sequence_length = sequence[:chords].length - 1
      approved_chords = get_indexes(sequence, chords.take(actual_sequence_length), chord)
      # Дальше, если ничего не нашло
      # puts "#{approved_chords}"
      next if approved_chords.length == 0 or approved_chords[0].length == 0
      # approved_chords = approved_chords[0].product(*approved_chords[1..-1])

      # Необходимо удалить дублирующийся аккорд между последовательностями
      sequence_chords = {
          :chord => chord,
          :sequence => [],
          :children => []
      }
      approved_chords.each_with_index do |chord_indexes, k|
        sequence_chords[:sequence] << chord_indexes.map {|index|
          {
              :chord => chords[k][index],
              :children => []
          }
        }
      end
      # sequence_chords[:sequence] = prepare_array(sequence_chords[:sequence])
      result << sequence_chords
      return prepare_array(result) if chords.drop(actual_sequence_length).empty?
      if sequence_chords[:sequence][-1].is_a? Array
        sequence_chords[:sequence][-1].each_with_index do |last_chord, i|
          sequence_chords[:children][i] = find_sequences_recursive(last_chord[:chord], chords.drop(actual_sequence_length))
        end
      else
        sequence_chords[:children] = find_sequences_recursive(sequence_chords[:sequence][-1][:chord], chords.drop(actual_sequence_length))
      end
    end
    prepare_array(result)
  end

  def prepare_array(array)
    while array.is_a? Array and array[0].is_a? Array and array.length == 1
      array = array[0]
    end
    array
  end

  def get_sequences_for_chord(chord)
    # Если аккорд - функция, то ищем для него, иначе смотрим функцию
    @sequences.find_all {|s| s[:chords][0] == _get_chord_id(chord)}
  end

  def get_indexes(sequence, prepared_chords, chord)
    result = []
    return [] if prepared_chords.length < sequence[:chords].length - 1
    # У всех аккордов под одним индексом одна и та же верхняя нота (сопрано)
    previous_chord = chord
    prepared_chords.each_with_index do |chords, i|
      sequence_chord = sequence[:chords][i+1]
      indexes = chords.size.times.select {|i|
        is_chord = _get_chord_id(chords[i]) == sequence_chord
        if sequence[:position]
          is_position = chords[i]["position"] == chord["position"]
        else
          is_position = true
        end
        # Если есть родительская функция, то она не должна совпадать
        # Есть её нет, то сам аккорд является функцией и он не должен совпадать с предыдущим
        if chords[i]["soprano"] == previous_chord["soprano"]
          is_another_chord = _get_chord_id(chords[i]) != _get_chord_id(previous_chord)
        else
          is_another_chord = true
        end

        # puts "#{chords[i].as_json}, #{is_chord}, #{is_position}"
        is_chord and is_position and is_another_chord
      }

      return [] if indexes.empty?
      result << indexes
      previous_chord = chords[indexes[-1]]
    end
    # Если self.to_array.length == 2, то мы возвращаем [ [1], [2], [3] ]
    # return result if self.to_array.length == 2
    # Если self.to_array.length == 2, то мы возвращаем [ [1, 4], [1, 5], [2, 5], [3, 6] ]

    result
  end

  def _get_chord_id(chord)
    chord["parent_chord_id"].nil? ? chord["id"] : chord["parent_chord_id"]
  end
end