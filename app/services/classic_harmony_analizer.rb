#encoding:utf-8

class ClassicHarmonyAnalizer < HarmonyAnalizer
  def initialize(tonality_id)
    @tonality = Tonality.find(tonality_id)
    @is_minor = !@tonality.is_major
    @available_first_chords = [
        Chord.find_by_name('T'),
    # Chord.find_by_name('T6'),
    ]
    @sequences = ChordSequence.get_all_sequences
  end

  def analyze_new(notes)
    prepared_chords = notes.map { |note|
      chords = Chord.where(:soprano => @tonality.get_stage(note))
      chords.each { |c| c.for_tonality(@tonality).with_soprano_octave(note[-1]) }
      chords
    }
    # Берем ИДы из объектов
    chord_ids = prepared_chords.map { |chords|
      chords.map { |c|
        {
            :id => c.id,
            :parent_id => c.parent_chord_id,
            :soprano_octave => c.soprano_octave
        }
      }
    }
    # Генерируем декартово произведение множеств
    chord_id_sequences = chord_ids[0].product(*chord_ids[1..-1])
    puts "#{chord_id_sequences}"
    # Получаем все подходящие последовательности аккордов (точнее, их ИДы)
    result = []
    chord_id_sequences.each do |chords|
      s = find_all_sequences(chords)
      next if s.length == 0
      puts "result sequence"
      puts "#{s}"
      puts "chords"
      puts "#{chords}"
      result << s
    end

    puts "#{result}"
    # result = chord_id_sequences.map{|chords| find_all_sequences(chords)}
    result.map { |sequences|
      sequences.map { |sequence|
        sequence.map { |ids|
          ids.map { |id|
            Chord.find(id[:id]).for_tonality(@tonality).with_soprano_octave(id[:soprano_octave]).convert
          }
        }
      }
    }
    # puts 'result'
    # result.each{|r|puts "#{r}"}

  end

  def find_all_sequences(chord_id_sequences)
    result = []

    find_sequences_recursive(chord_id_sequences, [], result)
    delete_wrong_sequences(result)
    if result.length > 0
      puts "result"
      puts "#{result}"
    end
    result
  end

  def delete_wrong_sequences(result_array)
    # Мы удаляем одно из найденных разбиений последовательностей,
    # Если одна из последовательностей в этом разбиении НЕ СОДЕРЖИТСЯ
    # В указанном нами ранее словаре таких последовательностей
    # Иначе говоря, если НЕ НАЙДЕТСЯ таких последовательностей, которые повторяли бы точь-в-точь
    # Рассматриваемую нами последовательность
    result_array.delete_if { |array|
      array.any? { |arr|
        not @sequences.any? { |seq_arr|
          seq_arr.size == arr.size and seq_arr.size.times.all? { |i|
            seq_arr[i] == arr[i][:id] or seq_arr[i] == arr[i][:parent_id]
          }
        }
      }
    }
  end

  def find_sequences_recursive(array, sequence, result)
    array.size.times.each do |i|
      prefix = array[0..i]
      next if prefix.length > 4 # Максимальная длина последовательности
      # puts "#{prefix}"
      next unless contains(prefix)
      deep_sequence = sequence.map { |s| s }
      deep_sequence << prefix
      # puts "#{sequence}"
      if i == array.size-1
        puts 'return'
        puts "#{deep_sequence}"
        result << deep_sequence
        return
      end
      find_sequences_recursive(array[i..-1], deep_sequence, result)
      # wordBreakUtil(str.substr(i, n-i), n-i, result + prefix + " ");
    end
  end


  def analyze(notes)
    prepared_chords = notes.map { |note|
      chords = Chord.where(:soprano => @tonality.get_stage(note)).to_a.shuffle
      chords.each { |c| c.with_soprano_octave(note[-1]).for_tonality(@tonality) }
      chords
    }

    chords = []
    i = 0
    j = 0
    loop do
      prev_i = i
      chord = prepared_chords[i][j]
      puts i, j, chord.to_json
      sequences = ChordSequence.get_sequences_for_chord(chord)
      puts sequences.length, sequences.to_json
      # Идем дальше
      # Если вся последовательность подходит, то добавляем сразу всё, иначе не добавляем ничего

      sequences.each do |sequence|
        approved_chords = sequence.get_indexes(prepared_chords[i+1..i+sequence.to_array.length-1])
        # Дальше, если ничего не нашло
        next if approved_chords.length == 0
        # Необходимо удалить дублирующийся аккорд между последовательностями
        chords.pop
        chords << chord.convert
        approved_chords.each_with_index do |chord_indexes, k|
          chords << prepared_chords[i+1+k][chord_indexes[0]].convert
        end
        # Бежим дальше по нотам
        i += approved_chords.length
        # Находим индекс того аккорда, который подошел
        j = approved_chords.length > 0 ? approved_chords[-1][0] : 0
        break
      end

      j+=1 if i == prev_i
      break if j == prepared_chords[i].length and i == prev_i # Значит последовательность некорректная
      break if chords.length == prepared_chords.length or i >= prepared_chords.length - 1 # Если дошли до конца
    end
    {
        :soprano => chords.map { |c| c[:soprano_note] },
        :alt => chords.map { |c| c[:alt_note] },
        :tenor => chords.map { |c| c[:tenor_note] },
        :bass => chords.map { |c| c[:bass_note] },
        :chords => chords.map { |c| c[:name] }
    }
  end

  def get_all_sequences
    source = [[1, 2], [3, 4, 5], [6, 7]]
    result = source[0].product(*source[1..-1])
    puts "#{result}"
  end

  def analyze_new_alg
    s = [1, 2, 3, 4, 5]

    word_break(s)
  end

  def contains(array)
    return false if array.length > 4
    dictionary = [[5, 59, 10, 12], [4, 59, 10, 12],
                  [1, 4, 7], [4, 3, 5], [5, 3, 4], [4, 15, 2],
                  [7, 15, 2], [1, 14, 2], [1, 9, 2], [2, 14, 1],
                  [8, 13, 1], [5, 13, 1], [2, 9, 1], [4, 8], [1, 8],
                  [1, 7], [5, 7], [2, 7], [1, 5], [1, 4], [2, 4],
                  [4, 2], [7, 2], [4, 1], [5, 1], [8, 1], [7, 1]]

    @sequences.any? { |arr|
      arr.size <= array.size and
          arr.size.times.all? { |i|
            arr[i] == array[i][:id] or arr[i] == array[i][:parent_id]
          }
    }
  end

  def contains_two(array)
    return false if array.length > 3
    dictionary = [[3, 4, 5], [1, 2, 3], [1, 2], [2, 3], [3, 4], [4, 5]]

    dictionary.any? { |arr|
      arr.size <= array.size and arr.size.times.all? { |i|
        arr[i] == array[i]
      }
    }
  end

  def word_break(array)
    # last argument is prefix
    # wordBreakUtil(str, str.size(), "");
    result = []
    word_break_util(array, [], result)

    a = [[1, 2], [2, 3], [3, 4, 5], [3, 4], [1, 2, 3], [4, 5]]
    # [1,2,3], [3,4,5]
    # [1,2,3], [3,4], [4,5]
    result.delete_if { |array| array.any? { |arr| not a.any? { |k| k == arr } } }
    puts 'result'
    result.each { |r| puts "#{r}" }
  end

  def word_break_util(array, sequence, result)
    array.size.times.each do |i|
      prefix = array[0..i]
      puts "#{prefix}"
      next unless contains_two(prefix)
      deep_sequence = sequence.map { |s| s }
      deep_sequence << prefix
      # puts "#{sequence}"
      # puts "#{deep_sequence}"
      if i == array.size-1
        puts 'return'
        result << deep_sequence
        return
      end
      word_break_util(array[i..-1], deep_sequence, result)
      # wordBreakUtil(str.substr(i, n-i), n-i, result + prefix + " ");
    end
  end

  private

  def self.simple_analyze
    h = ClassicHarmonyAnalizer.new(1)
    notes = %w(c4 d4 e4 f4)
    h.analyze(notes)
  end

  def self.new_alg
    h = ClassicHarmonyAnalizer.new(1)
    notes = %w(c4 d4 e4 f4)
    h.analyze_new(notes)
  end
end