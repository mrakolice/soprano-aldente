class ChordSequence < ActiveRecord::Base
  belongs_to :first_chord, class_name: "Chord"
  belongs_to :second_chord, class_name: "Chord"
  belongs_to :third_chord, class_name: "Chord"
  belongs_to :fourth_chord, class_name: "Chord"

  def self.get_sequences_for_chord(chord, is_json=false)
    # Если аккорд - функция, то ищем для него, иначе смотрим функцию
    parent_id = is_json ? chord["parent_chord_id"] : chord.parent_chord_id
    chord_id = is_json ? chord["id"] : chord.id

    result_chord_id = parent_id.nil? ? chord_id : parent_id
    ChordSequence.where(:first_chord_id => result_chord_id).order(fourth_chord_id: :desc, third_chord_id: :desc, second_chord_id: :desc).to_a
  end

  def get_indexes(prepared_chords, chord_position=false)
    result = []
    puts "#{self.as_json}"
    return [] if prepared_chords.length < self.to_array.length - 1
    prepared_chords.each_with_index do |chords, i|
      sequence_chord = self.to_array[i+1]
      # index = chords.find_index{|c|c.id == sequence_chord or c.parent_chord_id == sequence_chord}

      indexes = chords.size.times.
          select {|i|
        is_chord = (chords[i]["id"] == sequence_chord or chords[i]["parent_chord_id"] == sequence_chord)
        is_position = ((self.position and (chords[i]["position"] == chord_position)) or not self.position)
        puts "#{chords[i].as_json}, #{is_chord}, #{is_position}"
        is_chord and is_position
      }

      return [] if indexes.empty?
      result << indexes
    end
    # Если self.to_array.length == 2, то мы возвращаем [ [1], [2], [3] ]
    # return result if self.to_array.length == 2
    # Если self.to_array.length == 2, то мы возвращаем [ [1, 4], [1, 5], [2, 5], [3, 6] ]

    result
  end

  def self.get_all_sequences
    ChordSequence.all.order(fourth_chord_id: :desc, third_chord_id: :desc, second_chord_id: :desc).map{|c|{:chords => c.to_array, :position => c.position}}
  end

  def to_array
    result = []
    result << self.first_chord_id
    result << self.second_chord_id unless self.second_chord_id.nil?
    result << self.third_chord_id unless self.third_chord_id.nil?
    result << self.fourth_chord_id unless self.fourth_chord_id.nil?
    result
  end
end
