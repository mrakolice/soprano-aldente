class Tonality < ActiveRecord::Base

  def stages
    @stages ||= self.sequence.mb_chars.downcase.split(',')
                    .each_with_index.map{|note_name, index| Stage.new(note_name, index + 1, self.offset)}
  end

  def get_stage(note_name)
    stages.find{|s|s.note_name == note_name.mb_chars.downcase[0..-2]}.index
  end
end
