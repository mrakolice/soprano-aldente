#encoding:utf-8

class Stage
  include Comparable
  attr_accessor :index
  attr_accessor :note_name
  attr_accessor :offset

  def initialize(note_name, index, offset)
    # Считаем, что всегда будем оперировать ступенями внутри одной тональности
    @note_name = note_name
    @index = index
    @offset = offset
  end

  def get_note(octave)
    "#{note_name}#{octave}"
  end
  def <=>(another_stage)
    return 0 if self.index == another_stage.index
    offset1 = self.index + self.offset
    offset1 = offset1 - 8 if offset1 > 8

    offset2 = another_stage.index + another_stage.offset
    offset2 = offset2 - 8 if offset2 > 8

    if offset1 < offset2
      -1
    elsif offset1 > offset2
      1
    else
      0
    end
  end
end