class Chord < ActiveRecord::Base
  has_many :child_chords, class_name: "Chord", foreign_key: "parent_chord_id"
  belongs_to :parent_chord, class_name: "Chord"

  attr_accessor :soprano_stage
  attr_accessor :alt_stage
  attr_accessor :tenor_stage
  attr_accessor :bass_stage
  attr_accessor :soprano_octave

  def for_tonality(tonality)
    self.soprano_stage = tonality.stages.find{|s| s.index == self.soprano}
    self.alt_stage = tonality.stages.find{|s| s.index == self.alt}
    self.tenor_stage = tonality.stages.find{|s| s.index == self.tenor}
    self.bass_stage = tonality.stages.find{|s| s.index == self.bass}
    self
  end

  def with_soprano_octave(soprano_octave)
    self.soprano_octave = soprano_octave.to_i
    self
  end

  def convert
    r = {}
    octave = self.soprano_octave
    r[:soprano_note] = self.soprano_stage.get_note(octave)
    octave -= 1 if self.alt_stage >= self.soprano_stage
    r[:alt_note] = self.alt_stage.get_note(octave)
    octave -= 1 if self.tenor_stage >= self.alt_stage
    octave = 4 if octave > 4 # Хак, что делается октава меньше для корректного отображения
    r[:tenor_note] = self.tenor_stage.get_note(octave)
    octave -= 1 if self.bass_stage >= self.tenor_stage
    r[:bass_note] = self.bass_stage.get_note(octave)
    r[:name] = self.name
    r
  end

  def as_json(options=nil)
    result = super(options)
    result.merge(self.convert)
  end
end
