# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170509152603) do

  create_table "chord_sequences", force: :cascade do |t|
    t.integer  "first_chord_id",  limit: 4
    t.integer  "second_chord_id", limit: 4
    t.integer  "third_chord_id",  limit: 4
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.integer  "fourth_chord_id", limit: 4
    t.boolean  "position",                  default: true
  end

  add_index "chord_sequences", ["first_chord_id"], name: "fk_rails_5e150d5a62", using: :btree
  add_index "chord_sequences", ["fourth_chord_id"], name: "fk_rails_3fcb4aa4fb", using: :btree
  add_index "chord_sequences", ["second_chord_id"], name: "fk_rails_43aa878fe1", using: :btree
  add_index "chord_sequences", ["third_chord_id"], name: "fk_rails_22528f0986", using: :btree

  create_table "chords", force: :cascade do |t|
    t.string   "name",            limit: 255
    t.boolean  "position"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.integer  "parent_chord_id", limit: 4
    t.integer  "soprano",         limit: 4,   default: 1
    t.integer  "alt",             limit: 4,   default: 1
    t.integer  "tenor",           limit: 4,   default: 1
    t.integer  "bass",            limit: 4,   default: 1
  end

  add_index "chords", ["parent_chord_id"], name: "index_chords_on_parent_chord_id", using: :btree

  create_table "tonalities", force: :cascade do |t|
    t.text    "name",     limit: 65535
    t.text    "sequence", limit: 65535
    t.boolean "is_major",               default: true
    t.integer "offset",   limit: 4,     default: 0
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "chord_sequences", "chords", column: "first_chord_id"
  add_foreign_key "chord_sequences", "chords", column: "fourth_chord_id"
  add_foreign_key "chord_sequences", "chords", column: "second_chord_id"
  add_foreign_key "chord_sequences", "chords", column: "third_chord_id"
end
