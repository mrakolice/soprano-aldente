class AddFourthChordToChordSequence < ActiveRecord::Migration
  def change
    add_reference :chord_sequences, :fourth_chord
    add_foreign_key :chord_sequences, :chords, column: :fourth_chord_id
  end
end
