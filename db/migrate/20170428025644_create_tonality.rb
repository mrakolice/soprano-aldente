class CreateTonality < ActiveRecord::Migration
  def change
    create_table :tonalities do |t|
      t.text :name
      t.text :sequence
      t.boolean :is_major, default:true
    end
  end
end
