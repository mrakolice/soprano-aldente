class RemoveSequenceFromChord < ActiveRecord::Migration
  def change
    remove_column :chords, :sequence
  end
end
