class AddOffsetToTonalities < ActiveRecord::Migration
  def change
    add_column :tonalities, :offset, :integer, :default => 0
  end
end
