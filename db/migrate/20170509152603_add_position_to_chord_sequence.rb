class AddPositionToChordSequence < ActiveRecord::Migration
  def change
    add_column :chord_sequences, :position, :boolean, :default => true
  end
end
