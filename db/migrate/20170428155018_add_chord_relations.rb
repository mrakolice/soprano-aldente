class AddChordRelations < ActiveRecord::Migration
  def change
    add_reference :chords, :parent_chord, index: true
  end
end
