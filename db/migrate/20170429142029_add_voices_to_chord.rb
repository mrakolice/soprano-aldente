class AddVoicesToChord < ActiveRecord::Migration
  def change
    add_column :chords, :soprano, :integer, default: 1
    add_column :chords, :alt, :integer, default: 1
    add_column :chords, :tenor, :integer, default: 1
    add_column :chords, :bass, :integer, default: 1
  end
end
