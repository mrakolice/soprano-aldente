class CreateChordSequences < ActiveRecord::Migration
  def change
    create_table :chord_sequences do |t|
      t.references :first_chord, references: :chords
      t.references :second_chord, references: :chords
      t.references :third_chord, references: :chords

      t.timestamps null: false
    end

    add_foreign_key :chord_sequences, :chords, column: :first_chord_id
    add_foreign_key :chord_sequences, :chords, column: :second_chord_id
    add_foreign_key :chord_sequences, :chords, column: :third_chord_id
  end
end
