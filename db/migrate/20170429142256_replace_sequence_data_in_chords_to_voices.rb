class ReplaceSequenceDataInChordsToVoices < ActiveRecord::Migration
  def up
    Chord.all.each do |chord|
      voices = chord.sequence.split(',').map{|s|s.to_i}
      chord.update_attributes(
          :bass => voices[0],
          :tenor => voices[1],
          :alt => voices[2],
          :soprano => voices[3]
      )
    end
  end

  def down
    Chord.all.each do |chord|
      chord.sequence = "#{chord.bass},#{chord.tenor},#{chord.alt},#{chord.soprano}"
      chord.save
    end
  end
end
