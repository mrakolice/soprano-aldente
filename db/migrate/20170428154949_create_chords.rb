class CreateChords < ActiveRecord::Migration
  def change
    create_table :chords do |t|
      t.string :name
      t.string :sequence
      t.boolean :position

      t.timestamps null: false
    end
  end
end
